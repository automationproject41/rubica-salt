python-pip:
  pkg.installed

run-command:
  cmd.wait:
    - name: pip install ngxtop  >> /var/log/ngxtop.log
    - creates: /var/log/ngxtop.log
    - require:
      - pkg: python-pip
    - watch:
      - pkg: python-pip

