nginx:
  pkg.installed

deploy the http.conf file:
  file.managed:
    - name: /var/www/html/index.nginx-debian.html
    - source: salt://files/index.nginx-debian.html
