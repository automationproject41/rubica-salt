**Documentation of Changes made on servers for the Rubica Challenge**

**Security Changes**

**SSH**

   On both servers I set SSH authentication to certificate authentication only. I would have put ssh on port 2222, it seems I cannot reach the servers when they serving from this port. I am guessing there is a firewall in the way. I did not put these common configurations in salt as I could have lost connection to the servers. Normally I would run on a test server and then move forward with production. I can still put these in salt they would go in the common branch. 

**NGINX**
  
I went through this walk through to harden Nginx with the steps found here https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-on-ubuntu-14-04. I was also sure the latest packages were on the admin server. I did not however set up ssl or ip blocking on the nginx server. Here are the actions I did to harden Nginx.


1. Made sure that all packages were up to date.

2. Made sure that the Nginx package was up to date

3. Made sure Nginx name and version was not shown in the header.

4. Changed error pages so they could not be used by an attacker.

**Configuration Management**

 I used salt as my configuration management tool due to its security and flexibility. I set Server A as the configuration management server. I made a common branch salt minion configuration will be pushed to all servers in the base branch common. I then made an Nginx service that will be pushed to servers with a defined by the host name here all servers with web*. This would make all web servers made after this have nginx on them and it would be easy to stand up more.

I did run into a problem with salt hostname parsing when trying to use the host name of web*. Unfortunately AWS is sending the internal DNS name to the salt master this would be changed using this document
 https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html.
If you look in my top.sls file you can see the ugliness this created.

**My tree for salt looks like this**


common    – salt_minion.sls      goes to all servers


service   -  nginx.sls           goes only to web*


service   -  web_analytics.sls   put this on the web servers this would be moved to the admin server

**Web Server**

You can see the hello world page at http://54.202.2.143/

**Web Analytics**

This is where I ran into the most trouble. I am sure there is some slick cloud based web tool that will email beautiful analytics out. Google Analytics did not work in this case it sends at most once a day and the notifcation service is broken. I have a screen shot of that its not just me, everyone is having issues. So I rolled my own. So the first thing I found was a tool that parses nginx stats called nginxtop. This is a pip install so I had to do a little salt fu to get this to install properly. Next I needed a mail server to work off of. I used this walk through

https://www.linode.com/docs/email/postfix/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu.

This section also should be put into salt that would take about 30 minutes. I again did not want to affect server functionality here.

 I then tried for way to long to get cron to send the mail. I got the mail to send fine but it would always have no stats. The stats ran fine from command line. I got the email to send out using the gmail smtp servers. Unfortunatly during testing I got banned from the gmail outgoing servers. You can verify email is trying to go out using the command.
 
sudo cat /var/log/syslog | grep devops@rubica.com

   Here is the command I ran in a root screen session to get mail to go out
while true; do sudo /usr/local/bin/ngxtop --no-follow  | mail -s "Webstats" devops@rubica.com; sleep 7200; done

   Obviously I do not consider this a enterprise grade solution. I would look at using onsite outgoing mail servers or a cloud solution possibly AWS that does this sort of custom notification.

