salt-common:
  pkg.installed

salt-minion:
  pkg.installed

deploy the salt minion file:
  file.managed:
    - name: /etc/salt/minion
    - source: salt://files/minion
