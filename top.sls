base:
  '*':
    - common.salt_minion

  'ip-172-26-12-10*':
    - services.nginx
    - services.web_analytics
